package demo.com.styleitdemo.object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 5/31/2016.
 * Custom class for the articles
 */
public class ClothingArticle implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ClothingArticle> CREATOR = new Parcelable.Creator<ClothingArticle>() {
        @Override
        public ClothingArticle createFromParcel(Parcel in) {
            return new ClothingArticle(in);
        }

        @Override
        public ClothingArticle[] newArray(int size) {
            return new ClothingArticle[size];
        }
    };

    private String id;
    private String imageUrl;
    private String price;
    private String description;
    private String x0;
    private String x1;
    private String y0;
    private String y1;

    public ClothingArticle(String id, String imageUrl, String price, String description, String x0,
                           String x1, String y0, String y1) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.price = price;
        this.description = description;
        this.x0 = x0;
        this.x1 = x1;
        this.y0 = y0;
        this.y1 = y1;
    }

    protected ClothingArticle(Parcel in) {
        id = in.readString();
        imageUrl = in.readString();
        price = in.readString();
        description = in.readString();
        x0 = in.readString();
        x1 = in.readString();
        y0 = in.readString();
        y1 = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;

    }

    public String getX0() {
        return x0;
    }

    public void setX0(String x0) {
        this.x0 = x0;

    }

    public String getX1() {
        return x1;
    }

    public void setX1(String x1) {
        this.x1 = x1;

    }

    public String getY0() {
        return y0;
    }

    public void setY0(String y0) {
        this.y0 = y0;

    }

    public String getY1() {
        return y1;
    }

    public void setY1(String y1) {
        this.y1 = y1;

    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(imageUrl);
        dest.writeString(price);
        dest.writeString(description);
        dest.writeString(x0);
        dest.writeString(x1);
        dest.writeString(y0);
        dest.writeString(y1);
    }


}
