package demo.com.styleitdemo.object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 5/31/2016.
 * Custom class for the outfit
 */
public class Outfit implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Outfit> CREATOR = new Parcelable.Creator<Outfit>() {
        @Override
        public Outfit createFromParcel(Parcel in) {
            return new Outfit(in);
        }

        @Override
        public Outfit[] newArray(int size) {
            return new Outfit[size];
        }
    };
    private String imageUrl;
    private String height;
    private String width;
    private String id;

    public Outfit(String imageUrl, String id, String height, String width) {
        this.imageUrl = imageUrl;
        this.id = id;
        this.height = height;
        this.width = width;
    }

    protected Outfit(Parcel in) {
        imageUrl = in.readString();
        height = in.readString();
        width = in.readString();
        id = in.readString();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imageUrl);
        dest.writeString(height);
        dest.writeString(width);
        dest.writeString(id);
    }
}
