package demo.com.styleitdemo.object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 5/31/2016.
 * Custom class for the inspiration models
 */
public class InspirationModel implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<InspirationModel> CREATOR = new Parcelable.Creator<InspirationModel>() {
        @Override
        public InspirationModel createFromParcel(Parcel in) {
            return new InspirationModel(in);
        }

        @Override
        public InspirationModel[] newArray(int size) {
            return new InspirationModel[size];
        }
    };
    private String name;
    private String profileImageUrl;
    private String imageUrl;
    private String imageHeight;
    private String imageWidth;
    private String id;

    public InspirationModel(String name, String profileImageUrl, String imageUrl, String imageHeight,
                            String imageWidth, String id) {
        this.name = name;
        this.profileImageUrl = profileImageUrl;
        this.imageUrl = imageUrl;
        this.imageHeight = imageHeight;
        this.imageWidth = imageWidth;
        this.id = id;
    }

    protected InspirationModel(Parcel in) {
        name = in.readString();
        profileImageUrl = in.readString();
        imageUrl = in.readString();
        imageHeight = in.readString();
        imageWidth = in.readString();
        id = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(String imageHeight) {
        this.imageHeight = imageHeight;
    }

    public String getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(String imageWidth) {
        this.imageWidth = imageWidth;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(profileImageUrl);
        dest.writeString(imageUrl);
        dest.writeString(imageHeight);
        dest.writeString(imageWidth);
        dest.writeString(id);
    }
}
