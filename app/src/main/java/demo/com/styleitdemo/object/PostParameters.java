package demo.com.styleitdemo.object;

import java.util.HashMap;

/**
 * Created by Admin on 6/1/2016.
 * Custom class for the Post Requests
 */
public class PostParameters {

    private String url;
    private HashMap<String, String> params;

    public PostParameters(String url, HashMap<String, String> params) {
        this.url = url;
        this.params = params;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, String> getParams() {
        return params;
    }

    public void setParams(HashMap<String, String> params) {
        this.params = params;
    }

}
