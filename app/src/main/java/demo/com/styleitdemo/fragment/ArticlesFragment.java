package demo.com.styleitdemo.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import demo.com.styleitdemo.R;
import demo.com.styleitdemo.helper.JSONParser;
import demo.com.styleitdemo.helper.JSONReader;
import demo.com.styleitdemo.object.ClothingArticle;

/**
 * Class designed to show the different similiar Clothing Articles
 */
public class ArticlesFragment extends Fragment {

    /*
    Timer to load the inspiration models back
     */
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            mListener.timeoutAction();
        }
    };

    /*
    The similarClothingUrl and originalArticleFileName
     */
    private String similarClothingUrl;
    private ClothingArticle article;

    private ArticlesFragmentInteractionListener mListener;

    public ArticlesFragment() {
        // Required empty public constructor
    }

    /**
     * A filename is passed here as recommended by Craig. This is to provide a smoother UX by
     * allowing the pre-loaded data to be shown first creating the illusion of no loading screen
     *
     * @param url      Url of similar clothing URL
     * @param fileName the Filename of the original article to prevent excess loading
     * @return A new instance of fragment ArticlesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ArticlesFragment newInstance(String url, ClothingArticle article) {
        ArticlesFragment fragment = new ArticlesFragment();
        Bundle args = new Bundle();
        args.putString("similarClothingUrl", url);
        args.putParcelable("originalCloth", article);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            similarClothingUrl = getArguments().getString("similarClothingUrl");
            article = getArguments().getParcelable("originalCloth");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_articles, container, false);

        LinearLayout articleGallery = (LinearLayout) view.findViewById(R.id.articleGallery);

        final ImageView mainImageTemp = (ImageView) view.findViewById(R.id.articleImageView);
        final TextView articlePrice = (TextView) view.findViewById(R.id.articlePriceTextView);
        final TextView articleDescription = (TextView) view.findViewById(R.id.articleDescriptionTextView);

        /**
         * Load the pre-loaded data
         */
        Bitmap originalArticleBitmap = null;
        try {
            File bitmapFile = new File(getContext().getCacheDir(), article.getImageUrl());
            FileInputStream is = new FileInputStream(bitmapFile);
            originalArticleBitmap = BitmapFactory.decodeStream(is);
            is.close();
            bitmapFile.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Original article as first choice while others load
        final ImageView originalArticle = new ImageView(getContext());
        originalArticle.setImageBitmap(originalArticleBitmap);
        originalArticle.setAdjustViewBounds(true);
        originalArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                articlePrice.setText(article.getPrice());
                articleDescription.setText(article.getDescription());
                Bitmap bitmap = ((BitmapDrawable) originalArticle.getDrawable()).getBitmap();
                mainImageTemp.setImageBitmap(bitmap);
            }
        });
        articleGallery.addView(originalArticle);

        //Load the similar items
        JSONObject data = null;

        try {
            data = (new retrieveSimilarJSON().execute(similarClothingUrl).get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }


        JSONParser jsonParser = new JSONParser(data);
        List<ClothingArticle> similarArticles = null;
        try {
            similarArticles = jsonParser.parseSimilarItems();
        } catch (JSONException je) {
            je.printStackTrace();
        }

        /*
        Load the similar items
         */
        for (final ClothingArticle similarArticle : similarArticles) {
            final ImageView selectionArticle = new ImageView(getContext());
            selectionArticle.setAdjustViewBounds(true);
            Picasso.with(getContext()).load(similarArticle.getImageUrl() + "_2w").into(selectionArticle);
            selectionArticle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    articlePrice.setText(similarArticle.getPrice());
                    articleDescription.setText(similarArticle.getDescription());
                    Bitmap bitmap = ((BitmapDrawable) selectionArticle.getDrawable()).getBitmap();
                    mainImageTemp.setImageBitmap(bitmap);
                    resetTimer();
                }
            });
            articleGallery.addView(selectionArticle);
        }

        mainImageTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTimer();
                if (mainImageTemp.getDrawable() != null) {
                    Bitmap bitmap = ((BitmapDrawable) mainImageTemp.getDrawable()).getBitmap();
                    mListener.changeClothing(bitmap);
                }
            }
        });

        resetTimer();
        return view;
    }

    /*
    Used to reset the 10 second idle time which displays the models back into the screen
     */
    private void resetTimer() {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 15000);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;

        if (context instanceof Activity) {
            activity = (Activity) context;

            try {
                mListener = (ArticlesFragmentInteractionListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement ArticlesFragmentInteractionListener");
            }
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface ArticlesFragmentInteractionListener {
        void changeClothing(Bitmap article);

        void timeoutAction();
    }

    /*
    Retrieves the JSON by running a task in the background
    */
    private class retrieveSimilarJSON extends AsyncTask<String, Void, JSONObject> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        protected JSONObject doInBackground(String... strings) {
            //Read the json

            JSONReader jsonReader = new JSONReader(strings[0]);

            JSONObject jsonObject = jsonReader.retrieveJSONGet();


            return jsonObject;
        }

        protected void onPostExecute(JSONObject jsonObject) {

            //try {
                /*
                //Parse the json
                JSONParser jsonParser = new JSONParser();
                ArrayList<Question> questionArrayList = jsonParser.parseJSONfile(jsonArray);

                //Go to the next activity with the received questions
                */


            //launchDetailsActivity(questionArrayList);
            //} catch (JSONException je) {
            //    Log.e("ERROR", "getJsonInfo: ", je);
            //}
        }
    }

}
