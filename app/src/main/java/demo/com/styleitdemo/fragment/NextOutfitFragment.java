package demo.com.styleitdemo.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import demo.com.styleitdemo.R;

/**
 * NextOutfitFragment - Fragment that has a simple button for next outfit. I opted to make
 * it a fragment to potentially introduce additionally features such as like/dislike/favorite/share
 */
public class NextOutfitFragment extends Fragment {
    private NextOutfitFragmentListener mListener;

    public NextOutfitFragment() {
        // Required empty public constructor
    }

    public static NextOutfitFragment newInstance(String param1, String param2) {
        NextOutfitFragment fragment = new NextOutfitFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_next_outfit, container, false);
        Button nextOutfit = (Button) view.findViewById(R.id.nextOutfitButton);
        nextOutfit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.loadNewOutfit();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        if (context instanceof Activity) {
            activity = (Activity) context;

            try {
                mListener = (NextOutfitFragmentListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement NextOutfitFragmentListener");
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface NextOutfitFragmentListener {
        // TODO: Update argument type and name
        void loadNewOutfit();
    }
}
