package demo.com.styleitdemo.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import demo.com.styleitdemo.R;
import demo.com.styleitdemo.object.InspirationModel;
import demo.com.styleitdemo.view.InspirationView;


/**
 * ModelFragment Fragment which loads the inspiration models
 */
public class ModelFragment extends Fragment {

    //The list fo inspiration models
    private List<InspirationModel> inspirationModelList;

    public ModelFragment() {
        // Required empty public constructor
    }


    //models - A list of inspiration models
    public static ModelFragment newInstance(ArrayList<InspirationModel> models) {
        ModelFragment fragment = new ModelFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("inspirationModels", models);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            inspirationModelList = getArguments().getParcelableArrayList("inspirationModels");
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_model, container, false);

        //Add the inspirationModels into the layout
        LinearLayout modelsGallery = (LinearLayout) layout.findViewById(R.id.modelsGallery);
        for (final InspirationModel inspirationModel : inspirationModelList) {

            InspirationView model = new InspirationView(getContext(), inspirationModel);
            model.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder alertModel = new AlertDialog.Builder(getContext());
                    View popupView = inflater.inflate(R.layout.popup, null);
                    ImageView modelImageView = (ImageView) popupView.findViewById(R.id.popupModel);
                    //Load the full res image for popup
                    Picasso.with(getContext()).load(inspirationModel.getImageUrl()).into(modelImageView);

                    //Create the alertDialog
                    alertModel.setView(popupView);
                    final AlertDialog alertModelDialog = alertModel.show();

                    //Click the image to make it go away
                    modelImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertModelDialog.dismiss();
                        }
                    });
                }
            });
            modelsGallery.addView(model);
        }

        return layout;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
