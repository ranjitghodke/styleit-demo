package demo.com.styleitdemo.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import demo.com.styleitdemo.R;
import demo.com.styleitdemo.helper.MetricsCalculator;
import demo.com.styleitdemo.object.ClothingArticle;
import demo.com.styleitdemo.object.Outfit;


/**
 * OutfitFragment - Fragment responsible for displaying the initial outfit to the user
 */
public class OutfitFragment extends Fragment {
    List<ClothingArticle> clothingArticles;
    private Outfit outfit;
    private OutfitFragmentInteractionListener mListener;
    private List<ImageView> articleImageViewList;
    public OutfitFragment() {
        // Required empty public constructor
    }

    /**
     * Create a new fragment
     *
     * @param outfit   - the initial outfit
     * @param articles - the articles the outfit consists of
     * @return
     */
    public static OutfitFragment newInstance(Outfit outfit, ArrayList<ClothingArticle> articles) {
        OutfitFragment fragment = new OutfitFragment();
        Bundle args = new Bundle();
        args.putParcelable("outfit", outfit);
        args.putParcelableArrayList("clothingArticles", articles);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            outfit = getArguments().getParcelable("outfit");
            clothingArticles = getArguments().getParcelableArrayList("clothingArticles");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_outfit, container, false);

        GridLayout gridLayout = (GridLayout) layout.findViewById(R.id.originalOutfitLayout);

        articleImageViewList = new ArrayList<>();

        /*
        Add all of the clothing articles into the gridlayout and make their height divide their
        height by 2.5 to account for the weight of the fragment
         */
        for (final ClothingArticle x : clothingArticles) {
            final ImageView imageView = new ImageView(getContext());
            articleImageViewList.add(imageView);
            Float heightPercentage = (Float.parseFloat(x.getY1()) - Float.parseFloat(x.getY0()));
            Float widthPercentage = Float.parseFloat(x.getX1()) - Float.parseFloat(x.getX0());
            int height = (int) (heightPercentage * Float.parseFloat(outfit.getHeight()));
            int width = (int) (widthPercentage * Float.parseFloat(outfit.getWidth()));
            gridLayout.addView(imageView);
            imageView.getLayoutParams().height = (int) (MetricsCalculator.convertDpToPixel(height, getContext()) / 2.5);
            imageView.getLayoutParams().width = (int) (MetricsCalculator.convertDpToPixel(width, getContext()) / 2.5);
            Picasso.with(getContext()).load(x.getImageUrl() + "_2w").into(imageView); //Use _2w to speed up loading

            /*
            Set the appropriate actions on the appropriate clicks
             */
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (imageView.getBackground() != null) {
                        imageView.setBackground(null);
                        mListener.loadInspiration();
                    } else {
                        /**
                         * Set everything else to unselected
                         */
                        for (ImageView article1 : articleImageViewList) {
                            article1.setBackground(null);
                        }
                        imageView.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.imageview_border, null));
                        File file = null;
                        String fileName = null;
                        /*
                        Save the file to pre-load for later
                         */
                        try {
                            fileName = Uri.parse(outfit.getImageUrl()).getLastPathSegment();
                            file = File.createTempFile(fileName, null, getContext().getCacheDir());
                            FileOutputStream fOut = new FileOutputStream(file);
                            Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                            fOut.flush();
                            fOut.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        fileName = file.getName();
                        x.setImageUrl(fileName);
                        mListener.onArticleSelected(x.getId(), imageView, x);
                    }
                }
            });
        }
        return layout;
    }

    /*
    Removes the border
     */
    public void clearBackground(){
        for (ImageView article : articleImageViewList) {
            article.setBackground(null);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity;

        if (context instanceof Activity) {
            activity = (Activity) context;

            try {
                mListener = (OutfitFragmentInteractionListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement OutfitFragmentInteractionListener");
            }
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OutfitFragmentInteractionListener {
        void onArticleSelected(String id, ImageView imageView, ClothingArticle article);

        void loadInspiration();
    }
}
