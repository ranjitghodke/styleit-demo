package demo.com.styleitdemo.view;

import android.content.Context;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import demo.com.styleitdemo.R;
import demo.com.styleitdemo.object.InspirationModel;

/**
 * Created by Admin on 5/30/2016.
 * Custom view for the inspiration models
 */
public class InspirationView extends LinearLayout {

    public InspirationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.view_inspiration, this);
    }

    public InspirationView(Context context, final InspirationModel inspirationModel) {
        super(context);
        final LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.view_inspiration, this);

        final ImageView modelImage = (ImageView) layout.findViewById(R.id.modelImage);
        ImageView profilePic = (ImageView) layout.findViewById(R.id.profilePic);
        /*
        100x100 is default for profile pic
         */
        profilePic.setMaxHeight(100);
        profilePic.setMaxWidth(100);
        TextView modelName = (TextView) layout.findViewById(R.id.modelName);

        /*
        Checks if a lower resolution picture is available and if not get the original
         */
        Picasso.with(getContext()).load(inspirationModel.getImageUrl() + "_2w").into(modelImage, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onError() {
                Picasso.with(getContext()).load(inspirationModel.getImageUrl()).into(modelImage);
            }
        });

        try {
            //Have to find the redirect url
            String tempProfileUrl = new FindFinalURL().execute(inspirationModel.getProfileImageUrl()).get();
            Picasso.with(getContext()).load(tempProfileUrl).into(profilePic);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if (profilePic.getDrawable() == null) {
            profilePic.setImageResource(R.drawable.guest_img_profile);
        }

        modelName.setText(inspirationModel.getName());

    }

    /*
    Retrieve the final url
     */
    private class FindFinalURL extends AsyncTask<String, Void, String> {

        private static final int HTTP_OKAY = 200;
        //private static final int HTTP_DENIED = 403;

        protected String doInBackground(String... urls) {
            String urldisplay = urls[0];
            HttpURLConnection urlConnection;
            String urlString = null;
            try {

                URL url = new URL(urldisplay);
                //Get the text input from the url
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setInstanceFollowRedirects(false);

                int responseCode = urlConnection.getResponseCode();

               /*
               Logic to remove the append _2w in case of access denied
               Instead I am relying on Picasso
               See line 48
               */
                /*
                if(responseCode == HTTP_DENIED){
                    //Remove the last three chars
                    urlConnection.disconnect();
                    String newUrl = urls[0].substring(0, urls[0].length()-3);//Remove the "_2w"
                    url = new URL(newUrl);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setInstanceFollowRedirects(false);
                    responseCode = urlConnection.getResponseCode();
                }
                */

                if (responseCode != HTTP_OKAY) {
                    String newUrl = urlConnection.getHeaderField("Location");
                    url = new URL(newUrl);
                    //Get the text input from the url
                }

                urlString = url.toExternalForm();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return urlString;
        }

        protected void onPostExecute(String result) {
        }
    }

}


