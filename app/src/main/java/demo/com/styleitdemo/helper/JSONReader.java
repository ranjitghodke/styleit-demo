package demo.com.styleitdemo.helper;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.String;import java.lang.StringBuilder;import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Retrieves the JSON data from an URL. It works by retrieving the JSON data
 * in a buffered reader and passing the output to a StringBuilder. The StringBuilder then passing
 * that string into a JSONArray (or JSONObject) to create the returning object.
 */
public class JSONReader {

    private String url;
    private HashMap<String, String> params;

    //Empty constructor
    public JSONReader(String url, HashMap<String, String> params) {
        this.url = url;
        this.params = params;
    }

    //Empty constructor
    public JSONReader(String url) {
        this.url = url;
        this.params = null;
    }

    /*
    Returns a JSONArray object based on the data found at url via GET

    @param jsonURL  The url of the JSON
    @return         The JSONArray located at specified url
     */
    public JSONObject retrieveJSONGet() {
        InputStream inputStream = null;
        String result = null;
        HttpURLConnection urlConnection;
        String jsonURL = url;
        try {
            //Form a url from the string passed in
            URL url = new URL(jsonURL);
            //Get the text input from the url
            urlConnection = (HttpURLConnection) url.openConnection();
            inputStream = new BufferedInputStream(urlConnection.getInputStream());
            // read the json data that is UTF-8 by default
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"), 8);
            //Store the result into a string
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            result = sb.toString();
            //Disconnect the connection
            urlConnection.disconnect();

        } catch (IOException ioe) {
            Log.e("URL ERROR", "getWebInfo: ", ioe);
        } finally { //Close the inputStream
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException ioe) {
                Log.e("inputStream ERROR", "getWebInfo: ", ioe);
            }
        }
        try {
            //Return the JSONArray generated from the string retrieved
            JSONObject jObject = new JSONObject(result);
            return jObject;
        } catch (JSONException je) {
            Log.e("ERROR", "getJsonInfo: ", je);
            je.printStackTrace();
        }
        return null;
    }


    /*
    Returns a JSONObject object based on the data found at the url via POST.

    @return         The JSONObject located at specified url
     */
    public JSONObject retrieveJSONPost() {
        InputStream inputStream = null;
        String result = new String();
        HttpURLConnection urlConnection;
        String jsonURL = url;
        try {
            //Form a url from the string passed in
            URL url = new URL(jsonURL);
            //Get the text input from the url
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);

            OutputStream os = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(params));

            writer.flush();
            writer.close();
            os.close();
            int responseCode=urlConnection.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while ((line=br.readLine()) != null) {
                    result+=line;
                }
            }
            else {
                result="";

            }

        }
        catch (IOException ioe) {
            result = "{}";
            Log.e("URL ERROR", "getWebInfo: ", ioe);
        } finally { //Close the inputStream
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException ioe) {
                Log.e("inputStream ERROR", "getWebInfo: ", ioe);
            }
        }
        try {
            //Return the JSONArray generated from the string retrieved

            JSONObject jObject = new JSONObject(result);
            return jObject;
        } catch (JSONException je) {
            Log.e("ERROR", "getJsonInfo: ", je);
            je.printStackTrace();
        }
        return null;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

}
