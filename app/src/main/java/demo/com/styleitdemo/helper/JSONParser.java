package demo.com.styleitdemo.helper;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import demo.com.styleitdemo.object.ClothingArticle;
import demo.com.styleitdemo.object.InspirationModel;
import demo.com.styleitdemo.object.Outfit;

/**
 * Parses the JSON file to create the Questions and return them back in an ArrayList
 */
public class JSONParser {

    private JSONObject jsonObject;

    //Empty constructor
    public JSONParser(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    /*
    Goes through the JSONObject and returns image data
    @return String      The url of the completed outfit
     */
    public Outfit parseOutfitImageUrl() throws JSONException {

        JSONArray looks = jsonObject.getJSONArray("looks");
        JSONObject looksInfo = looks.getJSONObject(0);
        String imageUrl = looksInfo.getString("image_url");
        String id = looksInfo.getString("id");
        String height = looksInfo.getString("image_height");
        String width = looksInfo.getString("image_width");
        Outfit outfit = new Outfit(imageUrl, id, height, width);
        return outfit;
    }

    /*
    Goes through the JSONObject and returns inspiration_looks data
    @return List<InspirationModel>    All of the models
     */
    public List<InspirationModel> parseInspirationLooksArray() throws JSONException {
        JSONArray looks = jsonObject.getJSONArray("looks");
        JSONObject looksInfo = looks.getJSONObject(0);
        JSONArray inspirationLooks = looksInfo.getJSONArray("inspiration_looks");

        List<InspirationModel> inspirationModels = new ArrayList<InspirationModel>();
        for (int i = 0; i < inspirationLooks.length(); i++) {
            JSONObject data = inspirationLooks.getJSONObject(i);
            JSONObject creator = data.getJSONObject("creator");
            String name = creator.getString("name");
            name = (name.equals(" ")) ? "Fashionista" : name;
            String profileImageUrl = creator.getString("profile_image_url");
            String id = data.getString("id");
            String imageWidth = data.getString("image_width");
            String imageHeight = data.getString("image_height");
            String imageUrl = data.getString("image_url");
            InspirationModel model = new InspirationModel(name, profileImageUrl, imageUrl, imageHeight, imageWidth, id);
            inspirationModels.add(model);
        }

        return inspirationModels;
    }

    /*
    Goes through the JSONObject and returns products data
    @return List<ClothingArticle>    All of the products
    */
    public List<ClothingArticle> parseProductsArray() throws JSONException {
        JSONArray looks = jsonObject.getJSONArray("looks");
        JSONObject looksInfo = looks.getJSONObject(0);
        JSONArray productsArray = looksInfo.getJSONArray("products");

        List<ClothingArticle> clothingArticles = new ArrayList<ClothingArticle>();
        for (int i = 0; i < productsArray.length(); i++) {


            JSONObject data = productsArray.getJSONObject(i);
            String id = data.getString("id");
            String price = data.getString("price");
            String imageUrl = data.getString("image_url");
            String description = data.getString("description");
            String x0 = data.getString("x_0");
            String x1 = data.getString("x_1");
            String y0 = data.getString("y_0");
            String y1 = data.getString("y_1");


            ClothingArticle article = new ClothingArticle(id, imageUrl, price, description, x0,
                    x1, y0, y1);
            clothingArticles.add(article);
        }

        return clothingArticles;
    }

    /**
     * Goes through JSONObject and returns list of similar items
     */
    public List<ClothingArticle> parseSimilarItems() throws JSONException {
        List<ClothingArticle> similarClothingArticles = new ArrayList<>();
        JSONArray similarArticles = jsonObject.getJSONArray("products");
        for (int i = 0; i < similarArticles.length(); i++) {
            JSONObject similarCloth = similarArticles.getJSONObject(i);
            String id = similarCloth.getString("id");
            String price = similarCloth.getString("price");
            String brandName = similarCloth.getString("brand_name");
            String description = brandName + " " + similarCloth.getString("description");
            String imageUrl = similarCloth.getString("image_url");
            ClothingArticle similarClothing = new ClothingArticle(id, imageUrl, price, description,
                    null, null, null, null);
            similarClothingArticles.add(similarClothing);
        }
        return similarClothingArticles;
    }


}
