package demo.com.styleitdemo.activity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import demo.com.styleitdemo.R;
import demo.com.styleitdemo.fragment.ArticlesFragment;
import demo.com.styleitdemo.fragment.ModelFragment;
import demo.com.styleitdemo.fragment.NextOutfitFragment;
import demo.com.styleitdemo.fragment.OutfitFragment;
import demo.com.styleitdemo.helper.JSONParser;
import demo.com.styleitdemo.helper.JSONReader;
import demo.com.styleitdemo.object.ClothingArticle;
import demo.com.styleitdemo.object.InspirationModel;
import demo.com.styleitdemo.object.Outfit;
import demo.com.styleitdemo.object.PostParameters;

public class MainActivity extends FragmentActivity implements OutfitFragment.OutfitFragmentInteractionListener, ArticlesFragment.ArticlesFragmentInteractionListener, NextOutfitFragment.NextOutfitFragmentListener {

    /*
    Private variables used throughout the class
    modelFragment - The fragment with Inspiration Models
    outfitFragment - The fragment with the outfit
    jsonParser - The JSON parser
    outfit - Custom outfit object, please see object/Outfit.java
    clothingArticles - List of clothes in outfit
    inspirationModels - List of the inspiration models
    originalArticleImageView - The imageview of a selected article
    params - POST parameters
     */
    private ModelFragment modelFragment;
    private OutfitFragment outfitFragment;
    private JSONParser jsonParser;
    private Outfit outfit;
    private List<ClothingArticle> clothingArticles;
    private List<InspirationModel> inspirationModels;
    private ImageView originalArticleImageView;
    private HashMap<String, String> params;

    /**
     * onCreate
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        POST parameters
         */
        params = new HashMap<>();
        params.put("username", "xi-liu1");

        /*
        Create a new PostParameters object with the URL and params atached
         */
        PostParameters outfitPOSTInfo = new PostParameters("https://test.flaunt.peekabuy.com/api/look/create_looks_from_closet_with_anchors/", params);

        //Retrieve JSON at url
        new retrieveOutfitJSON().execute(outfitPOSTInfo);
    }

    /**
     * Called when user takes no action
     */
    public void timeoutAction(){
        outfitFragment.clearBackground();
        loadInspiration();
    }

    /**
     * Called to load the inspiration models
     */
    public void loadInspiration() {

        //Check if fragment is inspiration fragment
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.model_fragment_holder);
        if (f != null && f instanceof ModelFragment) {
            return; //no need to refresh
        }

        //Remove the articlesFragment if its there
        Fragment articlesFragment = getSupportFragmentManager().findFragmentByTag("articlesFragment");
        if (articlesFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(articlesFragment).commit();
        }
        //add after deleting
        getSupportFragmentManager().beginTransaction().add(modelFragment, "modelFragment").commit();
    }

    /**
     * Called when the user clicks new outfit
     */
    public void loadNewOutfit() {
        clearScreen(); //Clear the screen
        //Retrieve JSON info
        PostParameters outfitPOSTInfo = new PostParameters("https://test.flaunt.peekabuy.com/api/look/create_looks_from_closet_with_anchors/", params);
        new retrieveOutfitJSON().execute(outfitPOSTInfo);
    }

    /**
     * Finds the removes the necessary fragments
     */
    private void clearScreen() {
        Fragment outfitFragment = getSupportFragmentManager().findFragmentByTag("outfitFragment");
        Fragment modelFragment = getSupportFragmentManager().findFragmentByTag("modelFragment");
        Fragment articlesFragment = getSupportFragmentManager().findFragmentByTag("articlesFragment");

        if (outfitFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(outfitFragment).commit();
        }
        if (modelFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(modelFragment).commit();
        }
        if (articlesFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(articlesFragment).commit();
        }
    }

    /**
     * Loads a new outfit with the JSONObject
     *
     * @param jsonObject
     */
    private void loadOutfit(JSONObject jsonObject) {
        jsonParser = new JSONParser(jsonObject);
        try {
            outfit = jsonParser.parseOutfitImageUrl();
            clothingArticles = jsonParser.parseProductsArray();
            inspirationModels = jsonParser.parseInspirationLooksArray();
            createFragments();
            //Chose to not perform the AsyncTask in the loadOutfit to prevent the UX from being obstructed
            //new retrieveImageBitmap().execute(outfit.imageUrl);
        } catch (JSONException je) {
            je.printStackTrace();
            Toast.makeText(MainActivity.this, "Sorry, we are currently unavailable at this time", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * createFragments
     * Create new fragments with the appropriate parameters
     */
    private void createFragments() {

        /*
        Made some changes by opting to have multiple imageviews instead of a canvas that can be
        drawn on
         */

        /*
        String fileName = null;
        try {
            fileName = Uri.parse(outfit.imageUrl).getLastPathSegment();
            file = File.createTempFile(fileName, null, getCacheDir());
            FileOutputStream fOut = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

        }
        catch (IOException e) {
            Toast.makeText(MainActivity.this, "File creation went haywire", Toast.LENGTH_SHORT).show();
        }
        */

        // Create new Fragments to be placed in the activity layout
        outfitFragment = new OutfitFragment().newInstance(outfit, (ArrayList) clothingArticles);
        modelFragment = new ModelFragment().newInstance((ArrayList) inspirationModels);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.outfit_fragment_holder, outfitFragment, "outfitFragment").commit();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.model_fragment_holder, modelFragment, "modelFragment").commit();
    }

    /**
     * changeClothing
     *
     * @param cloth
     */
    public void changeClothing(Bitmap cloth) {
        //Put the new cloth into the outfit image
        originalArticleImageView.setImageBitmap(cloth);
    }

    /**
     * onArticleSelected - loads the selected article data
     *
     * @param id
     * @param imageView
     * @param article
     */
    public void onArticleSelected(String id, ImageView imageView, ClothingArticle article) {

        /**
         * A decision was made by Craig and I to load the data in a manner that doesn't interrupt
         * the UX
         */
        this.originalArticleImageView = imageView;

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("test.flaunt.peekabuy.com")
                .appendPath("api")
                .appendPath("look")
                .appendPath("get_similar_products")
                .appendPath("")
                .appendQueryParameter("username", "xi-liu1")
                .appendQueryParameter("product_id", id);
        String url = builder.build().toString();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        ArticlesFragment newFragment = new ArticlesFragment().newInstance(url, article);
        // Replace whatever is in the fragment_container view with this fragment,
        transaction.replace(R.id.model_fragment_holder, newFragment, "articlesFragment");
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }


    /*
    Retrieves the JSON by running a task in the background
    */
    private class retrieveOutfitJSON extends AsyncTask<PostParameters, Void, JSONObject> {
        private ProgressDialog mProgressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Stop User Interaction while loading
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setMessage("Getting Outfit...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(true);
            mProgressDialog.show();
        }

        protected JSONObject doInBackground(PostParameters... param) {
            //Read the json
            JSONReader jsonReader = new JSONReader(param[0].getUrl(), param[0].getParams());
            JSONObject jsonObject = jsonReader.retrieveJSONPost();
            return jsonObject;
        }

        protected void onPostExecute(JSONObject jsonObject) {
            mProgressDialog.dismiss(); //Remove the loading screen
            loadOutfit(jsonObject);
        }
    }

}

